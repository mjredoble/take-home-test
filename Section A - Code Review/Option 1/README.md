# Code Review: Option 1 (Python)

### In order to implement a class method in your SMSMessage, you need to be familiar with the syntax on how to define a class method in Python.
### Syntax: classmethod()

```
#Creation of class method
Class.methodName = classmethod(Class.methodName)
 
```

### Review: To achieve this in your code sample, the code below will show you how it was implemented: e.g test.py
```
class SMSMessage:
    message  = "Hello there!"

    def get_message(cls):
        print("message: ",cls.message)

#Creation of class method
SMSMessage.get_message = classmethod(SMSMessage.get_message) 

#Calling of class method
SMSMessage.get_message()
```


### Steps to run the sample code:
1. On the terminal Go to folder CoGrammer e.g cd CoGrammer
1. Go to folder Section A - Code Review
1. Type: python test.py - it will run the sample code attached
