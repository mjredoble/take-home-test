# This is a NodeJS + Express + MongoDB (Backend) with VueJS (Frontend) Project


### Application feature:
1. Registration
1. Login
1. Welcome Screen
1. See list of Movies
1. Add new Movie
1. Remove Movie



Frontend Repo:
https://gitlab.com/mjredoble/vue-demo

Working App (Frontend):
https://cogrammer-vue-frontend.herokuapp.com

Backend Repo:
https://gitlab.com/mjredoble/node-backend

Working App (Backend):
https://cogrammer-restapi.herokuapp.com/
