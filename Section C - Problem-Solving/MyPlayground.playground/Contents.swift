import UIKit
import Foundation
import XCTest

func calculateString(value: String) {
    var valueArr = value.components(separatedBy: " ")
    
    if let operatorString = valueArr.first {
        valueArr.remove(at: 0)
        
        switch operatorString {
        case "+":
            add(values: valueArr)
        case "-":
            subtract(values: valueArr)
        case "*":
            multiply(values: valueArr)
        case "/":
            divide(values: valueArr)
        case "factorial":
            fibonacci(values: valueArr)
        case "fibonacci":
            fibonacci(values: valueArr)
        default:
            print("UNKNOWN")
        }
    }
}

//MARK: - Convert to Double
func getDoubleValues(values: [String]) -> [Double] {
    var doubleValues = [Double]()
    
    for val in values {
        if let val = Double(val) {
            doubleValues.append(val)
        }
    }
    
    return doubleValues
}

//MARK: - Addition
func add(values: [String]) {
    var result: Double = 0.0
    
    for val in values {
        if let val1 = Double(val) {
            result = result + val1
        }
    }
    
    print("ADDITION RESULT: \(result)")
}

//MARK: - Subtraction
func subtract(values: [String]) {
    let doubleValues = getDoubleValues(values: values)
    var temp: Double = 0.0
    
    //Sort Descending
//    for a in 0..<doubleValues.count {
//        for b in 1..<doubleValues.count {
//            if doubleValues[a] < doubleValues[b] {
//                temp = doubleValues[a]
//                doubleValues[a] = doubleValues[b]
//                doubleValues[b] = temp
//            }
//        }
//    }
//
    temp = doubleValues.first!
    for b in 1..<doubleValues.count {
        temp = temp - doubleValues[b]
    }
    
    print("SUBTRACTION RESULT: \(temp)")
}

//MARK: - Multiply
func multiply(values: [String]) {
    let doubleValues = getDoubleValues(values: values)
    var temp: Double = 0.0
    
    temp  = doubleValues[0] * doubleValues[1]
    print("MULTIPLY RESULT: \(temp)")
}

//MARK: - Divide
func divide(values: [String]) {
    let doubleValues = getDoubleValues(values: values)
    var temp: Double = 0.0
    
    temp  = doubleValues[0] / doubleValues[1]
    print("DIViDE RESULT: \(temp)")
}

func factorial(values: [String]) {
    guard let number = getDoubleValues(values: values).first else {
        return
    }
    
    let val = Int(number)
    var numberSet = [Int]()
    
    for x in 1...val {
        numberSet.append(x)
    }
    
    var temp = 0
    for a in 0..<numberSet.count {
        for b in 1..<numberSet.count {
            //print("\(numberSet[a]) * \(numberSet[b])")
            temp = a * b
        }
    }

//    print(temp)
}

func fibonacci(values: [String]) {
    guard let number = getDoubleValues(values: values).first else {
        return
    }
    
    var num1 = 0
    var num2 = 1
    let val = Int(number)
    
    for _ in 2...val {
        let num = num1 + num2
        num1 = num2
        num2 = num
    }
    
    print("result: = \(num2)")
}

calculateString(value: "+ 12.5 12.5")
calculateString(value: "- 12.5 13.5")
calculateString(value: "* 6 -12")
calculateString(value: "/ 20 2")

//TEST: "- 12.5 12.5" = 0
//TEST: "+ 12.5 12.5" = 25
//TEST: "- 12.5 13.5" = 1
