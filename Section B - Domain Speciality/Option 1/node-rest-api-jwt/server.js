const express = require('express'); 
const logger = require('morgan');
const PORT = process.env.PORT || 3000;

//ROUTES
const movies = require('./routes/movies') ;
const users = require('./routes/users') ;

//DATABASE CONFIGURATION
const mongoose = require('./config/database'); 

const app = express();

var jwt = require('jsonwebtoken');

// JWT SECRET TOKEN
app.set('secretKey', 'nodeRestApi'); 

//MONGODB CONNECTION
mongoose.connection.on('error', console.error.bind(console, 'MongoDB connection error:'));

app.use(logger('dev'));
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.get('/', function(req, res){
    res.json({"message" : "Section B - Web Application"});
});

// public route
app.use('/users', users);

// private route
app.use('/movies', validateUser, movies);

app.get('/favicon.ico', function(req, res) {
    res.sendStatus(204);
});

function validateUser(req, res, next) {
    jwt.verify(req.headers['x-access-token'], req.app.get('secretKey'), function(err, decoded) {
        if (err) {
            res.json({status:"error", message: err.message, data:null});
          }
          else{
            // add user id to request
            req.body.userId = decoded.id;
            next();
          }
    });    
}

// express doesn't consider not found 404 as an error so we need to handle 404 explicitly
// handle 404 error
app.use(function(req, res, next) {
    let err = new Error('Not Found - XXXX');
    err.status = 404;
    next(err); 
});

// handle errors
app.use(function(err, req, res, next) {
    console.log(err);
    
    if(err.status === 404)
        res.status(404).json({message: "Status Code: 404"});
    else 
        res.status(500).json({message: "Status Code: 500"});
});

app.listen(PORT, function() { 
    console.log('Node server listening on port 3000');
});