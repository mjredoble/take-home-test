
class SMSMessage:
    message  = "Hello there!"

    def get_message(cls):
        print("message: ",cls.message)

#Creation of class method
SMSMessage.get_message = classmethod(SMSMessage.get_message) 

#Calling of class method
SMSMessage.get_message()